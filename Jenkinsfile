#!groovy

pipeline {
    agent none

    triggers {
        pollSCM('H/2 9-20 * * 0-5')
    }

    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(daysToKeepStr: '7'))
    }

    parameters {
        choice(choices: 'patch\nminor\nmajor', description: 'Release type', name: 'VERSION_TYPE') 
        string(defaultValue: '1.0.0', description: 'Version with three-part numeric format (e.g. 1.0.0)', name: 'VERSION')
        string(defaultValue: '', description: 'e.g. alpha, rc1', name: 'VERSION_METADATA')
    }

    stages {
        stage('Checkin') {
            when {
                environment name: 'ENABLE_BITBUCKET_STATUS_NOTIFY', value: 'true'
            }
            steps {
                timestamps {
                    bitbucketStatusNotify(buildState: 'INPROGRESS', credentialsId: 'JenkinsOAuth', buildKey: '$BUILD_ID', buildName: '$BUILD_TAG', buildDescription: 'Build in progress...')
                }
            }
        }
        stage('Build App') {
            agent  {
                docker {
                    image 'bitninja/phpunit'
                    args  '--user=root:root --volume=$WORKSPACE:/home --volume=$WORKSPACE/../../.ssh:/root/.ssh --workdir=/home'
                    alwaysPull true
                }
            } 
            steps {
                timestamps {
                    sh "composer pre-build" 
                    sh "echo 'Downloading project dependencies...'"
                    sh "composer install --no-progress"
                }
            }
        }
        stage('Metrics') {
            agent  {
                docker {
                    image 'bitninja/phpunit'
                    args  '--user=root:root --volume=$WORKSPACE:/home --volume=$WORKSPACE/../../.ssh:/root/.ssh --workdir=/home'
                    alwaysPull true
                }
            } 
            steps {
                timestamps {
                    sh "echo 'Syntax analyzis...'"
                    sh "composer phplint"
                    sh "echo 'Static code analyzis...'"
                    sh "composer phpqa"
                    sh "echo 'Fixing phpmd and phpcpd file paths in generated .xml...'"
                    sh(""" sed -i "s@\$(pwd)@.@" build/phpmd.xml """)
                    sh(""" sed -i "s@\$(pwd)@.@" build/phpcpd.xml """)
                    sh "echo 'Security check...'"
                    //retry(3) {
                    //    sh "composer security-check"
                    //}
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build/phpmetrics', reportFiles: 'index.html', reportName: 'PHP Metrics', reportTitles: ''])
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build', reportFiles: 'phploc.html', reportName: 'PHP LOC', reportTitles: ''])
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build', reportFiles: 'phpcpd.html', reportName: 'PHP CPD', reportTitles: ''])
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'build', reportFiles: 'pdepend.html', reportName: 'PDepend', reportTitles: ''])
                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: 'build/checkstyle.xml', unHealthy: ''
                    pmd canComputeNew: false, defaultEncoding: '', healthy: '', pattern: 'build/phpmd.xml', unHealthy: ''
                    dry defaultEncoding: '', failedNewHigh: '10', failedTotalHigh: '20', healthy: '', highThreshold: 30, normalThreshold: 15, pattern: 'build/phpcpd.xml', thresholdLimit: 'high', unHealthy: '', unstableNewHigh: '5', unstableTotalHigh: '10', useStableBuildAsReference: true
                    openTasks defaultEncoding: '', excludePattern: '**/vendor/**', failedTotalHigh: '3', failedTotalLow: '100', failedTotalNormal: '15', healthy: '', high: '@fixme', low: '@deprecated', normal: '@todo, TODO', pattern: '**/*.php', unHealthy: '', unstableTotalLow: '50', unstableTotalNormal: '10', useStableBuildAsReference: true
                    step([$class: 'AnalysisPublisher', canComputeNew: false, defaultEncoding: '', healthy: '', unHealthy: ''])
                    lastChanges since: 'LAST_SUCCESSFUL_BUILD'
                }
            }
        }
        stage('Test') {
            agent  {
                docker {
                    image 'bitninja/phpunit'
                    args  '--user=root:root --volume=$WORKSPACE:/home --volume=$WORKSPACE/../../.ssh:/root/.ssh --workdir=/home'
                    alwaysPull true
                }
            } 
            steps {
                timestamps {
                    sh "echo 'Running unit tests...'"
                    sh "composer test"
                    step([
                        $class: 'CloverPublisher',
                        cloverReportDir: 'tests/_output',
                        cloverReportFileName: 'coverage.xml',
                        healthyTarget: [methodCoverage: 70, conditionalCoverage: 80, statementCoverage: 80],
                        unhealthyTarget: [methodCoverage: 50, conditionalCoverage: 50, statementCoverage: 50],
                        failingTarget: [methodCoverage: 0, conditionalCoverage: 0, statementCoverage: 0]
                    ])
                    xunit testTimeMargin: '3000', thresholdMode: 2, thresholds: [failed(failureNewThreshold: '1', failureThreshold: '3', unstableNewThreshold: '1', unstableThreshold: '2'), skipped(unstableNewThreshold: '1', unstableThreshold: '2')], tools: [PHPUnit(deleteOutputFiles: true, failIfNotNew: true, pattern: 'tests/_output/report.xml', skipNoTestFiles: false, stopProcessingIfError: true)]
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'tests/_output/coverage', reportFiles: 'index.html', reportName: 'Coverage', reportTitles: ''])
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'tests/_output', reportFiles: 'report.html', reportName: 'Test report', reportTitles: ''])
                }
            }
        }
        stage('Versioning') {
            when {
                environment name: 'ENABLE_VERSIONING', value: 'true'
            }
            agent any
            steps {
                timestamps {
                    sh "chmod +x versioning.sh"
                    sh "./versioning.sh"
                    sshagent (credentials: ['CIOpenSSHKey']) {
                        sh "git tag -a \$(./versioning.sh) -m 'Jenkins'"
                        sh('git push $GIT_URL --tags')
                    }
                }
            }
        }
        stage('Leave') {
            when {
                environment name: 'ENABLE_BITBUCKET_STATUS_NOTIFY', value: 'true'
            }
            steps {
                timestamps {

                }
            }
            post {
                success {
                    timestamps {
                        bitbucketStatusNotify(buildState: 'SUCCESSFUL', credentialsId: 'JenkinsOAuth', buildKey: '$BUILD_ID', buildName: '$BUILD_TAG', buildDescription: 'Build successfully finished!')
                    }
                }
                failure {
                    timestamps {
                        bitbucketStatusNotify(buildState: 'FAILED', credentialsId: 'JenkinsOAuth', buildKey: '$BUILD_ID', buildName: '$BUILD_TAG', buildDescription: 'Build failed!')
                    }
                }
            }
        }
    }
}