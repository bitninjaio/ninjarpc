<?php
/**
 * This file is part of the bitninja.ninjarpc
 *
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace BitNinja\NinjaRpc\Exception;

class LogicException extends \LogicException implements ExceptionInterface
{
}
