<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Exception;

class ResultNotArrivedException extends LogicException
{
}
