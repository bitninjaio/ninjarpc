<?php
/**
 * This file is part of the bitninja.ninjarpc
 *
 * @license http://opensource.org/licenses/MIT MIT
 */
namespace bitninja\ninjarpc\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
