<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Exception;

use BitNinja\NinjaRpc\CallHandler;

class CallTimeoutException extends LogicException
{
    /**
     * @var CallHandler
     */
    private $call;

    /**
     * Set the timeouted call handler
     *
     * @param CallHandler $callHandler
     */
    public function setCallHandler(CallHandler $callHandler)
    {
        $this->call = $callHandler;
    }

    /**
     * Get the timeouted call handler.
     *
     * @return CallHandler
     */
    public function getCallHandler()
    {
        return $this->call;
    }
}
