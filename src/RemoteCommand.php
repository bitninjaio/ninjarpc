<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc;

//use Drefined\Zipkin\Core\Identifier;

class RemoteCommand
{
    /**
     * The name of the servie
     *
     * @var string
     */
    private $serviceName;

    /**
     * The name of the called method/controller
     *
     * @var string
     */
    private $methodName;

    /**
     * Parameters
     *
     * @var array
     */
    private $params;

    /**
     * The name of the response queue. The server will publish
     * the result in this queue.
     *
     * @var string
     */
    private $responseQueueName;

    /**
     * There is only one response queue per clinet.
     * The correlationId is responsible for indentifying the
     * different calls form the same client.
     *
     * @var string
     */
    private $correlationId;

    /**
     * Value of the results
     *
     * @var mixed
     */
    private $result;

    /**
     * The following are related to tracing the call by
     * http://zipkin.io/pages/instrumenting.html
     *
     * For the initial receipt of a request no trace information exists.
     * So we create a trace id and span id. These should be 64 random bits.
     * The span id can be the same as the trace id.
     *
     * If the request already has trace information attached to it, the service
     * should use that information as server receive and server send events are
     * part of the same span as the client send and client receive events
     *
     * @var integer
     */
    private $traceId;

    /**
     * or the initial receipt of a request no trace information exists.
     * So we create a trace id and span id. These should be 64 random bits.
     * The span id can be the same as the trace id.
     *
     * @var integer
     */
    private $spanId;

    /**
     * Parent span id
     * If the service calls out to a downstream service a new span is created
     * as a child of the former span. It is identified by the same trace id
     * , a new span id, and the parent id is set to the span id of the previous
     * span. The new span id should be 64 random bits.
     *
     * @var integer
     */
    private $parentId;

    /**
     * Lets the downstream service know if it should record trace information
     * for the request.
     *
     * @var boolean
     */
    private $sampled;

    /**
     * Provides the ability to create and communicate feature flags. This is
     * how we can tell downstream services that this is a “debug” request.
     *
     * @var
     */
    private $flags;

    /**
     * @param type $serviceName
     * @param type $methodName
     * @param type $params
     * @param type $responseQueueName
     */
    public function __construct($serviceName, $methodName, $params, $responseQueueName)
    {
        $this->correlationId = uniqid();
        $this->serviceName = $serviceName;
        $this->methodName = $methodName;
        $this->params = $params;
        $this->responseQueueName = $responseQueueName;
        //$this->traceId = (string) Identifier::generate();
        //$this->spanId = (string) Identifier::generate();
        $this->traceId = null;
        $this->spanId = null;
    }

    /**
     * Return with any array which represent this command.
     */
    public function toArray()
    {
        $res['serviceName'] = $this->serviceName;
        $res['methodName'] = $this->methodName;
        $res['params'] = $this->params;
        $res['responseQueueName'] = $this->responseQueueName;
        $res['correlationId'] = $this->correlationId;
        $res['result'] = $this->result;
        $res['traceId'] = $this->traceId;
        $res['spanId'] = $this->spanId;
        $res['parnetId'] = $this->parentId;
        $res['sampled'] = $this->sampled;
        $res['flags'] = $this->flags;

        return $res;
    }

    public function loadFromArray($array)
    {
        $this->serviceName = $array['serviceName'];
        $this->methodName = $array['methodName'];
        $this->params = $array['params'];
        $this->responseQueueName = $array['responseQueueName'];
        $this->correlationId = $array['correlationId'];
        $this->result = $array['result'];
        $this->traceId = $array['traceId'];
        $this->spanId = $array['spanId'];
        $this->parentId = $array['parnetId'];
        $this->sampled = $array['sampled'];
        $this->flags = $array['flags'];
    }

    /**
     * Return the correlationId of this RPC command.
     *
     * @return string
     */
    public function getCorrelationId()
    {
        return $this->correlationId;
    }

    /**
     * The name of the service
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * The name of the method called.
     *
     * @return string
     */
    public function getMethodName()
    {
        return $this->methodName;
    }

    /**
     * Parameters
     *
     * @return type
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * The name of the response queue. The server will publish
     * the result in this queue.
     *
     * @return string
     */
    public function getResponseQueueName()
    {
        return $this->responseQueueName;
    }

    /**
     * The return value of the called RPC
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set the RPC result. Used by the server
     *
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * The following are related to tracing the call by
     * http://zipkin.io/pages/instrumenting.html
     *
     * For the initial receipt of a request no trace information exists.
     * So we create a trace id and span id. These should be 64 random bits.
     * The span id can be the same as the trace id.
     *
     * If the request already has trace information attached to it, the service
     * should use that information as server receive and server send events are
     * part of the same span as the client send and client receive events
     *
     * @return integer
     */
    public function getTraceId()
    {
        return $this->traceId;
    }

    /**
     * or the initial receipt of a request no trace information exists.
     * So we create a trace id and span id. These should be 64 random bits.
     * The span id can be the same as the trace id.
     *
     * @return integer
     */
    public function getSpanId()
    {
        return $this->spanId;
    }

    /**
     * Lets the downstream service know if it should record trace information
     * for the request.
     *
     * @return integer
     */
    public function getParnetId()
    {
        return $this->parentId;
    }

    /**
     * Lets the downstream service know if it should record trace information
     * for the request.
     *
     * @return type
     */
    public function isSampled()
    {
        return $this->sampled;
    }

    /**
     * Provides the ability to create and communicate feature flags. This is
     * how we can tell downstream services that this is a “debug” request.
     *
     * @return type
     */
    public function getFlags()
    {
        return $this->flags;
    }

    public function setTraceId($traceId)
    {
        $this->traceId = $traceId;
    }

    public function setSpanId($spanId)
    {
        $this->spanId = $spanId;
    }

    public function setParnetId($parentId)
    {
        $this->parentId = $parentId;
    }

    public function setSampled($sampled)
    {
        $this->sampled = $sampled;
    }

    public function setFlags($flags)
    {
        $this->flags = $flags;
    }
}
