<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */

namespace BitNinja\NinjaRpc;

use BitNinja\NinjaRpc\Encoders\EncoderInterface;
use BitNinja\NinjaRpc\Event\Emitter;
use BitNinja\NinjaRpc\Event\EventType;
use BitNinja\NinjaRpc\QueueManagers\QueueManagerInterface;
use BitNinja\NinjaRpc\Routers\RouterInterface;
use Psr\Log\LoggerAwareInterface;

class Server implements LoggerAwareInterface
{
    /**
     * @var QueueManagerInterface
     */
    private $queueManager;

    /**
     * @var EncoderInterface
     */
    private $encoder;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $log;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * The Event emitter.
     *
     * @var Emitter
     */
    private $emitter;

    /**
     * Build your server by varying the queu manager and encoder.
     *
     * @param string                $serviceName
     * @param QueueManagerInterface $queueManager
     * @param EncoderInterface      $encoder
     * @param RouterInterface       $router
     * @param Emitter               $emitter
     * @param int                   $heartbeat
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function __construct($serviceName, QueueManagerInterface $queueManager, EncoderInterface $encoder, RouterInterface $router, Emitter $emitter = null, $heartbeat = 60)
    {
        $this->queueManager = $queueManager;
        $this->encoder = $encoder;
        $this->router = $router;
        $this->emitter = $emitter;

        $services = $this->router->getServices();
        foreach ($services as $service => $callback) {
            $queueNames[] = $serviceName . '_' . $service;
        }
        if ($this->queueManager->getHeartbeat() === 0) {
            $this->queueManager->setHeartbeat($heartbeat);
        }
        $this->queueManager->connect();
        $this->queueManager->setListeningQueues($queueNames);
        $this->queueManager->initServiceConsumption();
    }

    /**
     * Listen for an incoming call and invoke the service defined by the
     * router object then send back the result.
     *
     * * @param string $exchange
     */
    public function listen($exchange = '')
    {
        $message = false;
        while ($message == false) {
            // Wait for a message or 1 second.
            $this->queueManager->waitWithTimeout();
            $message = $this->queueManager->getNext();
        }

        if (isset($this->log)) {
            $this->log->debug('New message arrived. Length of the message is [' . strlen($message) . '] byte');
        }
        $command = $this->encoder->decode($message);

        if ($this->emitter !== null) {
            $this->emitter->raiseEvent(EventType::SERVER_AFTER_CALL_ARRIVED, $command);
        }

        $result = $this->router->invoke($command);
        $command->setResult($result);

        if ($this->emitter !== null) {
            $this->emitter->raiseEvent(EventType::SERVER_BEFORE_RESPONSE_SENT, $command);
        }
        // Send back the result.
        $this->queueManager->initResponseQueue($command->getResponseQueueName());
        $this->queueManager->publish($command->getResponseQueueName(), $this->encoder->encode($command), $exchange);
        if ($this->emitter !== null) {
            $this->emitter->raiseEvent(EventType::SERVER_AFTER_RESPONSE_SENT, $command);
        }
    }

    /**
     * Inject a logger
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->log = $logger;
    }
}
