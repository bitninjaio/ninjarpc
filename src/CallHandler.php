<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc;

use BitNinja\NinjaRpc\RemoteCommand;
use Psr\Log\LoggerAwareInterface;

class CallHandler implements LoggerAwareInterface
{
    /**
     * Holds if the result has arried
     *
     * @var boolean
     */
    private $replyArrived = false;

    /**
     * The result
     *
     * @var mixed
     */
    private $result = null;

    /**
     * Logger to use.
     *
     * @var Psr\Log\LoggerAwareInterface
     */
    private $log;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * The RemoteCommand object which was used to carry the RPC call.
     *
     * @var RemoteCommand
     */
    private $command;

    /**
     * Timeout in seconds
     *
     * @var integer
     */
    private $timeout;

    /**
     * The timestemp when the call was sent.
     *
     * @var float
     */
    private $startTimestamp;

    /**
     * Don't use it directly. The Client can create this objec tfor you
     * by calling the asyncCall method.
     *
     * @param \BitNinja\NinjaRpc\Client $client
     * @param RemoteCommand             $command
     * @param integer                   $timeout
     */
    public function __construct(Client $client, RemoteCommand $command, $timeout = 60)
    {
        $this->client = $client;
        $this->command = $command;
        $this->timeout = $timeout;
        $this->startTimestamp = microtime(true);
    }

    /**
     * Wait synchronously for the response of the call represented by this
     * object. It will call the client object wait method until it recieves
     * a response for this specific call.
     */
    public function wait()
    {
        while (! $this->replyArrived) {
            $this->client->wait($this->command,$this);
            $this->replyArrived = $this->client->isResultArrived($this->command->getCorrelationId());
        }

        $this->result = $this->client->getResult($this->command->getCorrelationId());
        
        return $this->result;
    }

    /**
     * Inject a logger
     *
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->log = $logger;
    }

    /**
     * Get result of a request
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Use it for debugging purposes, to var_dump tze result.
     */
    public function dumpResult()
    {
        if (! isset($this->log)) {
            return;
        }

        if ($this->replyArrived) {
            $this->log->debug(var_export($this->result, true));

            return;
        }

        $this->log->info('No repliy has arrived yet!');
    }

    /**
     * If the call was timed out then it retunrs true.
     * Else it returns false.
     */
    public function isTimeouted()
    {
        $now = microtime(true);
        $difference = ($now - $this->startTimestamp);
        if ($difference > $this->timeout) {
            return true;
        }

        return false;
    }
}
