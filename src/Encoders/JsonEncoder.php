<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Encoders;

use BitNinja\NinjaRpc\RemoteCommand;
use Psr\Log\LoggerAwareInterface;

class JsonEncoder implements EncoderInterface, LoggerAwareInterface
{
    /**
     * @param type $encodedString
     *
     * @return RemoteCommand
     */
    public function decode($encodedString)
    {
        $decodedArray = json_decode($encodedString, true);
        $command = new RemoteCommand('', '', [], '');
        $command->loadFromArray($decodedArray);

        return $command;
    }

    public function encode(RemoteCommand $command)
    {
        $array = $command->toArray();

        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->log = $logger;
    }
}
