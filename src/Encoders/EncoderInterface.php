<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BitNinja\NinjaRpc\Encoders;

use BitNinja\NinjaRpc\RemoteCommand;

/**
 * @author Zsolt
 */
interface EncoderInterface
{
    /**
     * @param RemoteCommand $command
     *
     * @return string The encoded string
     */
    public function encode(RemoteCommand $command);

    /**
     * @param string $encodedString
     *
     * @return RemoteCommand The decoded command
     */
    public function decode($encodedString);
}
