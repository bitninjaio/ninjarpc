<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Encoders;

use BitNinja\NinjaRpc\RemoteCommand;
use Psr\Log\LoggerAwareInterface;

/**
 * Don't use this encoder in case of a non trusted clien because of the
 * security vulnerability regarding decoding objects
 * http://php.net/manual/en/function.unserialize.php
 *
 * @deprecated
 */
class PhpEncoder implements EncoderInterface, LoggerAwareInterface
{
    private $log;

    /**
     * @param type $encodedString
     *
     * @return RemoteCommand
     */
    public function decode($encodedString)
    {
        return unserialize($encodedString);
    }

    public function encode(RemoteCommand $command)
    {
        $result = serialize($command);
        if (isset($this->log)) {
            $this->log->debug('Encoded result string is ['.$result.']');
        }

        return $result;
    }

    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->log = $logger;
    }
}
