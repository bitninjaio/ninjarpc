<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace BitNinja\NinjaRpc\Routers;

use BitNinja\NinjaRpc\RemoteCommand;

/**
 * @author Zsolt
 */
interface RouterInterface
{
    /**
     * You can use this to retrive all available service names.
     *
     * @return array an array of the available services
     */
    public function getServices();

    /**
     * Invoke the command.
     *
     * @param RemoteCommand $command
     */
    public function invoke(RemoteCommand $command);
}
