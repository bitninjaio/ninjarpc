<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Routers;

use BitNinja\NinjaRpc\RemoteCommand;

class SimpleRouter implements RouterInterface
{
    private $services;

    public function __construct($services)
    {
        $this->services = $services;
    }

    public function getServices()
    {
        return $this->services;
    }

    public function invoke(RemoteCommand $command)
    {
        $result = call_user_func_array(
            $this->services[$command->getMethodName()],
            $command->getParams()
        );

        return $result;
    }

//put your code here
}
