<?php

/**
 * This file is part of the bitninja.ninjarpc
 *
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace BitNinja\NinjaRpc;

use BitNinja\NinjaRpc\CallHandler;
use BitNinja\NinjaRpc\Encoders\EncoderInterface;
use BitNinja\NinjaRpc\Event\Emitter;
use BitNinja\NinjaRpc\Event\EventType;
use BitNinja\NinjaRpc\Exception\CallTimeoutException;
use BitNinja\NinjaRpc\Exception\ResultNotArrivedException;
use BitNinja\NinjaRpc\QueueManagers\QueueManagerInterface;
use BitNinja\NinjaRpc\RemoteCommand;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

class Client implements LoggerAwareInterface
{
    /**
     * @var QueueManagerInterface
     */
    private $queueManager;

    /**
     * @var EncoderInterface
     */
    private $encoder;

    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * A unique id for the client instance
     *
     * @var string
     */
    private $clientId;

    /**
     * List of results.
     *
     * @var array
     */
    private $results;

    /**
     * The RPC timeout in seconds
     *
     * @var integer
     */
    private $timeout = 60;

    /**
     * Array of the calls currently invoked.
     *
     * @var array
     */
    private $ongoingCalls;

    /**
     * The Event emitter.
     *
     * @var Emitter
     */
    private $emitter;

    /**
     * @param QueueManagerInterface $queueManager
     * @param EncoderInterface      $encoder
     * @param Emitter               $emitter
     */
    public function __construct(QueueManagerInterface $queueManager, EncoderInterface $encoder, Emitter $emitter = null)
    {
        $this->queueManager = $queueManager;
        $this->queueManager->connect();
        $this->encoder = $encoder;
        $this->clientId = uniqid();
        $this->emitter = $emitter;
    }

    /**
     * Initialize an async call. The method returns right after sending the
     * call to the remote service. We return a CallHandler instance.
     * You can use this objec to monitor the result of the call.
     *
     * @param string $serviceName
     * @param string $methodName
     * @param array  $parameters
     * @param string $exchange
     *
     * @return CallHandler
     */
    public function asyncCall($serviceName, $methodName, $parameters = [], $exchange = '')
    {
        $queueName = $serviceName . '_' . $methodName;
        $responseQueueName = $queueName . '_' . $this->clientId;
        $command = new RemoteCommand($serviceName, $methodName, $parameters, $responseQueueName);

        $message = $this->encoder->encode($command);
        $this->queueManager->initServiceQueue($queueName);

        if ($this->emitter !== null) {
            $this->emitter->raiseEvent(EventType::CLIENT_BEFORE_CALL_SENT, $command);
        }

        $this->queueManager->publish($queueName, $message, $exchange);

        if ($this->emitter !== null) {
            $this->emitter->raiseEvent(EventType::CLIENT_AFTER_CALL_SENT, $command);
        }

        // this 3 lines cause low proccessing when we wont wanna wait the response
        /* $this->queueManager->setListeningQueues([$responseQueueName]);
        $this->queueManager->initResponseConsumption();
        $this->ongoingCalls[$command->getCorrelationId()] = $call; */

        $call = new CallHandler($this, $command, $this->timeout);

        return $call;
    }

    /**
     * Set the timeout for the RPC calls.
     * Default value is 60
     *
     * @param type $timeoutInSecs
     */
    public function setTimeout($timeoutInSecs)
    {
        $this->timeout = $timeoutInSecs;
    }

    /**
     * Inject a logger to use.
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->log = $logger;
    }

    /**
     * Returns true if we received a result for the call identified by the
     * given correlation id. In normal cases it is used by a CallHandler
     * object.
     *
     * @param string $correlationId
     *
     * @return boolean
     */
    public function isResultArrived($correlationId)
    {
        if (isset($this->results[$correlationId])) {
            return true;
        }

        return false;
    }

    /**
     * Get the result of a request. In normal cases it is used by
     * a CallHandler object to get fetch the result.
     *
     * @param type $correlationId
     *
     * @return mixed
     */
    public function getResult($correlationId)
    {
        if (isset($this->results[$correlationId])) {
            return $this->results[$correlationId];
        }
        throw new ResultNotArrivedException();
    }

    /**
     * PLEASE DO NOT CALL THAT FUNCTION DIRECTLY FROM CLIENT!!!
     * USE CALLHANDLER->WAIT()!!!!
     * 
     * The callHandler will call that function
     * Wait for a responses from any of the calls
     * @param $command BitNinja\NinjaRpc\RemoteCommand
     * @param $callHander BitNinja\NinjaRpc\CallHandler
     */
    public function wait($command, $callHandler)
    {
        $responseQueueName = $command->getResponseQueueName();
        $this->queueManager->setListeningQueues([$responseQueueName]);
        $this->queueManager->initResponseConsumption();

        $this->ongoingCalls[$command->getCorrelationId()] = $callHandler;

        $result = false;
        while ($result == false) {
            // Wait for a message or 1 second.
            $this->queueManager->waitWithTimeout();
            $this->checkTimeouts();
            $nextMessage = $this->queueManager->getNext();

            if ($nextMessage !== false) {
                $result = $this->encoder->decode($nextMessage);
                if (($this->emitter !== null) && ($result !== null)) {
                    $this->emitter->raiseEvent(EventType::CLIENT_AFTER_RESPONSE_ARRIVED, $result);
                }
                $this->results[$result->getCorrelationId()] = $result;
                unset($this->ongoingCalls[$result->getCorrelationId()]);
            }
        }

        // Load up any other messages too.
        while ($nextMessage = $result = $this->queueManager->getNext()) {
            $result = $this->encoder->decode($nextMessage);
            $this->results[$result->getCorrelationId()] = $result;
            unset($this->ongoingCalls[$result->getCorrelationId()]);
        }
    }

    /**
     * Chec if any calls have timeouted and throw an exception if
     * there is.
     */
    protected function checkTimeouts()
    {
        if (!isset($this->ongoingCalls)) {
            return;
        }

        foreach ($this->ongoingCalls as $key => $call) {
            if ($call->isTimeouted()) {
                unset($this->ongoingCalls[$key]);
                $ex = new CallTimeoutException();
                $ex->setCallHandler($call);
                throw $ex;
            }
        }
    }
}
