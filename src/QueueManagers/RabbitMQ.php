<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */

namespace BitNinja\NinjaRpc\QueueManagers;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerAwareInterface;
use PhpAmqpLib\Connection\Heartbeat\PCNTLHeartbeatSender;

/**
 * @SuppressWarnings(PHPMD)
 */
class RabbitMQ implements QueueManagerInterface, LoggerAwareInterface
{
    private $sendConnection;
    private $sendChannel;
    private $recvConnection;
    private $recvChannel;
    private $host;
    private $port;
    private $username;
    private $password;
    private $vhost;
    private $log;
    private $listeningQueues;
    private $arrivedMessages;
    private $initializedResponseQueues;
    private $initializedServiceQueues;
    private $serviceComsumptionInitialized = false;
    private $responseConsumptionInitialized = false;
    private $heartbeat;

    public function __construct($host = '127.0.0.1', $port = 5672, $username = 'guest', $password = 'guest', $vhost = '/', $heartbeat = 0)
    {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->vhost = $vhost;
        $this->heartbeat = $heartbeat;
    }

    public function connect()
    {
        $this->initializedResponseQueues = [];
        $this->initializedServiceQueues = [];

        $this->sendConnection = AMQPStreamConnection::create_connection([
            ['host' => $this->host, 'port' => $this->port, 'user' => $this->username, 'password' => $this->password, 'vhost' => $this->vhost]
        ], ['heartbeat' => 0]);

        $this->recvConnection = AMQPStreamConnection::create_connection([
            ['host' => $this->host, 'port' => $this->port, 'user' => $this->username, 'password' => $this->password, 'vhost' => $this->vhost]
        ], ['heartbeat' => $this->heartbeat]);

        if ($this->heartbeat !== 0) {
            $recvSender = new PCNTLHeartbeatSender($this->recvConnection);
            $recvSender->register();
        }

        $this->sendChannel = $this->sendConnection->channel();
        $this->recvChannel = $this->recvConnection->channel();

        if (isset($this->log)) {
            $this->log->info('RabbitMq connections established.');
        }
    }

    public function disconnect()
    {
        $this->sendChannel->close();
        $this->sendConnection->close();

        $this->recvChannel->close();
        $this->recvConnection->close();
    }

    public function setListeningQueues($queueNames)
    {
        $this->listeningQueues = $queueNames;
    }

    public function initServiceConsumption()
    {
        if (!isset($this->listeningQueues)) {
            return;
        }

        foreach ($this->listeningQueues as $queueName) {
            $this->initServiceQueue($queueName);
            $this->recvChannel->basic_consume($queueName, '', false, true, false, false, [$this, 'callback']);
        }

        $this->serviceComsumptionInitialized = true;
    }

    public function initResponseConsumption()
    {
        if (!isset($this->listeningQueues)) {
            return;
        }

        foreach ($this->listeningQueues as $queueName) {
            $this->initResponseQueue($queueName);
            $this->recvChannel->basic_consume($queueName, '', false, true, false, false, [$this, 'callback']);
        }

        $this->responseConsumptionInitialized = true;
    }

    /**
     * Wait for messages to arrive, or return after 1 second.
     * Returns after at least 1 new message has arrived or
     * 1 second have passed.
     */
    public function waitWithTimeout()
    {
        try {
            $this->recvChannel->wait(null, false, 1);
        } catch (AMQPTimeoutException $ex) {
        } catch (AMQPRuntimeException $ex) {
            // disconnected from server. Reconnecting.
            $this->log->error('Disconnected from RabbitMQ server. Trying to reconnect.');
            $this->connect();

            if ($this->responseConsumptionInitialized) {
                $this->initResponseConsumption();
            }

            if ($this->serviceComsumptionInitialized) {
                $this->initServiceConsumption();
            }
        }
    }

    /**
     *
     */
    public function hasNext()
    {
        return $this->hasNext;
    }

    /**
     * Get the next message or return FALSE if there is no message.
     */
    public function getNext()
    {
        if (!is_array($this->arrivedMessages)) {
            return false;
        }

        return count($this->arrivedMessages) > 0 ? array_shift($this->arrivedMessages) : false;
    }

    public function callback(AMQPMessage $message)
    {
        $this->arrivedMessages[] = $message->getBody();
    }

    public function publish($queueName, $message, $exchange = '')
    {
        $msg = new AMQPMessage($message);
        $this->sendChannel->basic_publish($msg, $exchange, $queueName);
        if (isset($this->log)) {
            $this->log->info('Message publised to [' . $queueName . '] queue');
        }
    }

    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->log = $logger;
    }

    public function initResponseQueue($queueName)
    {
        if (isset($this->initializedResponseQueues[$queueName])) {
            return;
        }

        /*
         * http://www.rabbitmq.com/amqp-0-9-1-quickref.html#queue.declare
         * https://www.rabbitmq.com/ttl.html
         * Declare an exclusive, auto-delete queue with a ttl of 60 sec (1 min)
         */
        $this->sendChannel->queue_declare(
            $queueName,
            false,
            false,
            false,
            true,
            false,
            ['x-expires' => ['I', 60000]]
        );
        $this->initializedResponseQueues[$queueName] = 1;
    }

    public function initServiceQueue($queueName)
    {
        if (isset($this->initializedServiceQueues[$queueName])) {
            return;
        }

        /*
         * http://www.rabbitmq.com/amqp-0-9-1-quickref.html#queue.declare
         * Declare an auto-delete queue
         */

        $this->sendChannel->queue_declare($queueName, false, false, false, true, false);
        $this->initializedServiceQueues[$queueName] = 1;
    }

    public function declareExchange($exchange, $type)
    {
        $this->sendChannel->exchange_declare($exchange, $type, false, false, false);
    }

    public function bindQueue($queueName, $exchange, $routing_key)
    {
        $this->sendChannel->queue_bind($queueName, $exchange, $routing_key);
    }

    public function getHeartbeat()
    {
        return $this->heartbeat;
    }

    public function setHeartbeat(int $heartbeat)
    {
        $this->heartbeat = $heartbeat;
    }
}
