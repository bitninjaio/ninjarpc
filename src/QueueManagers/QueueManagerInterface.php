<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BitNinja\NinjaRpc\QueueManagers;

/**
 * @author Zsolt
 */
interface QueueManagerInterface
{
    /**
     * Initiate the connections
     */
    public function connect();

    /**
     * Close the connections
     */
    public function disconnect();

    /**
     * publish a message to a queue
     *
     * @param string $queueName
     * @param string $message
     * @param string $exchange
     */
    public function publish($queueName, $message, $exchange);

    /**
     * Retrive the next received message
     */
    public function getNext();

    /**
     * Wait for messages to arrive, or return after 1 second.
     * Returns after at least 1 new message has arrived or
     * 1 second have passed.
     */
    public function waitWithTimeout();

    /**
     * Initialize a queue to use as service queue
     * Call this before using a queue as service queue
     *
     * @param type $queueName
     */
    public function initServiceQueue($queueName);

    /**
     * Initialize a queue to use for receiving response messages.
     * Call this before using a queue as response queue.
     *
     * @param type $queueName
     */
    public function initResponseQueue($queueName);

    /**
     * Initialize service queue consumption
     */
    public function initServiceConsumption();

    /**
     * Initialize response queue consumptioon
     */
    public function initResponseConsumption();

    /**
     * Set up the queues this queue manager will listen to
     *
     * @param array $queueNames
     */
    public function setListeningQueues($queueNames);

    /**
     * Getting heartbeat property
     * @return int
     */
    public function getHeartbeat();

    /**
     * Setting heartbeat
     * @param int $heartbeat
     */
    public function setHeartbeat(int $heartbeat);
}
