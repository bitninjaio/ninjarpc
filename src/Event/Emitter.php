<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Event;

use BitNinja\NinjaRpc\RemoteCommand;

class Emitter implements EmitterInterface
{
    private $eventHandlers;

    public function raiseEvent($event, RemoteCommand &$command)
    {
        foreach ($this->eventHandlers as $handler) {
            $handler->onEvent($event, $command);
        }
    }

    public function register(EventHandlerInterface $eventHandler)
    {
        $this->eventHandlers[] = $eventHandler;
    }
}
