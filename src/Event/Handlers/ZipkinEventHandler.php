<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Event\Handlers;

use BitNinja\NinjaRpc\Event\EventHandlerInterface;
use BitNinja\NinjaRpc\Event\EventType;
use BitNinja\NinjaRpc\RemoteCommand;
use Drefined\Zipkin\Core\Annotation;
use Drefined\Zipkin\Core\BinaryAnnotation;
use Drefined\Zipkin\Core\Identifier;
use Drefined\Zipkin\Core\Span;

class ZipkinEventHandler implements EventHandlerInterface
{
    /**
     * @var ZipkinManager
     */
    private $zipkinManager;

    /**
     * @var boolean
     */
    private $debugMode = false;

    /**
     * @var RemoteCommand
     */
    private $command;

    /**
     * @param \BitNinja\NinjaRpc\Event\Handlers\ZipkinManager $traceManager
     */
    public function __construct(ZipkinManager $traceManager)
    {
        $this->zipkinManager = $traceManager;
        $this->debugMode = false;
    }

    public function setDebugMode($mode)
    {
        $this->debugMode = $mode;
    }

    public function onEvent($event, RemoteCommand &$command)
    {
        $this->command = & $command;

        if ($event == EventType::CLIENT_AFTER_CALL_SENT) {
            $this->onClientAfterCall();
        }

        if ($event == EventType::SERVER_AFTER_CALL_ARRIVED) {
            $this->onServerAfterCallArrived();
        }

        if ($event == EventType::SERVER_AFTER_RESPONSE_SENT) {
            $this->onServerAfterResponseSent();
        }

        if ($event == EventType::CLIENT_AFTER_RESPONSE_ARRIVED) {
            $this->onClientAfterResponseArrived();
        }
    }

    public function onClientAfterCall()
    {
        $trace = $this->zipkinManager->getTraceWithNewSpan(
            $this->command,
            $this->command->getTraceId(),
            $this->command->getSpanId()
        );
        //$trace = $this->zipkinManager->getTraceForCommand($this->command);
        //$trace->pushSpan($this->zipkinManager->getSpan($this->command));

        $binaryAnnotations = [];

        if ($this->debugMode) {
            $binaryAnnotations = $this->getParamsAsBinaryAnnotations();
        }

        $trace->record(
            [Annotation::generateClientSend()],
            $binaryAnnotations
        );

        //$trace->popSpan();
    }

    public function onServerAfterCallArrived()
    {
        $trace = $this->zipkinManager->getTraceWithNewSpan(
            $this->command,
            new Identifier($this->command->getTraceId()),
            new Identifier($this->command->getSpanId())
        );

        $span = $trace->popSpan();
        $newSpan = new Span(
            $span->getName(),
            new Identifier($this->command->getTraceId()),
            new Identifier($this->command->getSpanId()),
            null,
            [],
            [],
            $span->getDebug(),
            $span->getTimestamp(),
            $span->getDuration()
        );

        $trace->pushSpan($newSpan);

        $trace->record(
            [\Drefined\Zipkin\Core\Annotation::generateServerRecv()],
            []
        );
    }

    public function onServerAfterResponseSent()
    {
        $trace = $this->zipkinManager->getTraceForCommand($this->command);

        $trace->record(
            [\Drefined\Zipkin\Core\Annotation::generateServerSend()],
            []
        );
        $trace->popSpan();
    }

    public function onClientAfterResponseArrived()
    {
        $trace = $this->zipkinManager->getTraceForCommand($this->command);

        $trace->record(
            [\Drefined\Zipkin\Core\Annotation::generateClientRecv()],
            []
        );
        $trace->popSpan();
    }

    protected function getParamsAsBinaryAnnotations()
    {
        $binaryAnnotations = [];
        $params = $this->command->getParams();
        foreach ($params as $name => $value) {
            $binaryAnnotations[] = BinaryAnnotation::generateString(
                $name,
                substr(var_export($value, true), 0, 1024)
            );
        }

        return $binaryAnnotations;
    }
}
