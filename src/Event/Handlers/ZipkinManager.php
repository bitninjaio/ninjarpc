<?php

namespace BitNinja\NinjaRpc\Event\Handlers;

use BitNinja\NinjaRpc\RemoteCommand;
use Drefined\Zipkin\Core\Endpoint;
use Drefined\Zipkin\Core\Identifier;
use Drefined\Zipkin\Core\Trace;
use Drefined\Zipkin\Tracer;
use Drefined\Zipkin\Transport\HTTPLogger;
use GuzzleHttp\Client;

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
class ZipkinManager
{
    private $client;
    private $logger;
    private $tracer;

    /**
     * @var array
     */
    private $endpoints;

    public function __construct($zipkinServerAddress = 'http://zipkin-server:9411/api/v1/spans')
    {
        $this->client   = new Client();
        $this->logger   = new HTTPLogger($this->client, $zipkinServerAddress);
        $this->tracer   = new Tracer($this->logger, 1.0, true);
    }

    /**
     * Return a Trace object with a new span.
     *
     * @param RemoteCommand $command
     *
     * @return Trace
     */
    public function getTraceWithNewSpan(RemoteCommand $command, string $traceId, string $spanId)
    {
        $endpoint = $this->getEndpoint($command);
        $trace = $this->getTrace($endpoint, $command);
        $trace->createNewSpan(
            $command->getMethodName(),
            new Identifier($traceId),
            new Identifier($spanId),
            null,
            round(microtime(true) * 1000000)
        );

        return $trace;
    }

    public function getTraceForCommand($command)
    {
        $endpoint = $this->getEndpoint($command);
        $trace = $this->getTrace($endpoint, $command);

        return $trace;
    }

    /**
     * @param Endpoint      $endpoint
     * @param RemoteCommand $command
     *
     * @return Trace
     */
    protected function getTrace(Endpoint $endpoint, RemoteCommand $command)
    {
        if (! isset($this->traces[$command->getServiceName()])) {
            $this->traces[$command->getServiceName()] = new Trace($this->tracer, $endpoint);
        }

        return $this->traces[$command->getServiceName()];
    }

    /**
     * @param RemoteCommand $command
     *
     * @return Endpoint
     */
    protected function getEndpoint(RemoteCommand $command)
    {
        if (! isset($this->endpoints[$command->getServiceName()])) {
            $this->endpoints[$command->getServiceName()] = new Endpoint('127.0.0.1', 80, $command->getServiceName());
        }

        return $this->endpoints[$command->getServiceName()];
    }
}
