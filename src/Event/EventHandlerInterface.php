<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BitNinja\NinjaRpc\Event;

use BitNinja\NinjaRpc\RemoteCommand;

/**
 * @author Zsolt
 */
interface EventHandlerInterface
{
    public function onEvent($event, RemoteCommand &$command);
}
