<?php
/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Event;

class EventType
{
    const CLIENT_BEFORE_CALL_SENT = 1;
    const CLIENT_AFTER_CALL_SENT = 2;
    const CLIENT_AFTER_RESPONSE_ARRIVED = 3;

    const SERVER_AFTER_CALL_ARRIVED = 100;
    const SERVER_BEFORE_RESPONSE_SENT = 101;
    const SERVER_AFTER_RESPONSE_SENT = 102;
}
