<?php

/**
 * @author     Zsolt Egri <ezsolt@bitninja.io>
 * @copyright  (c) 2014, Web-Server Kft
 *
 * @version    1.0
 */
namespace BitNinja\NinjaRpc\Test;

use BitNinja\NinjaRpc\Encoders\PhpEncoder;
use BitNinja\NinjaRpc\RemoteCommand;
use Monolog\Logger;

use PHPUnit\Framework\TestCase;

class PhpEncoderTest extends TestCase
{
    /**
     * @var JsonEncoder
     */
    private $underTest;

    public function setUp()
    {
        $this->underTest = new PhpEncoder();
        $this->underTest->setLogger($this->createMock(Logger::class));
    }

    public function testEncodeRemoteCommand()
    {
        // GIVEN in setUp()
        $command = new RemoteCommand('TestService', 'method1', ['param1' => 'value1'], 'TestService_method1_aa');

        // WHEN
        $encoded = $this->underTest->encode($command);

        // THEN
        $expected = serialize($command);
        $this->assertEquals($expected, $encoded);
    }

    public function testDecodeRemoteCommand()
    {
        // GIVEN in setUp()
        $command = new RemoteCommand('TestService', 'method1', ['param1' => 'value1'], 'TestService_method1_aa');
        $encoded = serialize($command);

        // WHEN
        $decoded = $this->underTest->decode($encoded);

        // THEN

        $this->assertEquals($command, $decoded);
    }
}
