<?php

namespace BitNinja\NinjaRpc\Test;

use BitNinja\NinjaRpc\CallHandler;
use BitNinja\NinjaRpc\Client;
use BitNinja\NinjaRpc\RemoteCommand;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class CallHandlerTest extends TestCase
{
    private $underTest;
    private $client;
    private $command;

    public function setUp()
    {
        parent::setUp();
        $this->client = $this->createMock(Client::class);
        $this->command = $this->createMock(RemoteCommand::class);
        $this->underTest = new CallHandler($this->client, $this->command);
    }

    /**
     * MethodName_DoesWhat_WhenTheseConditions
     */
    public function testWaitCallsClientWait()
    {
        // GIVEN in setUp()
        $this->client->method('isResultArrived')
                ->willReturn(true);

        // WILL
        $this->client->expects($this->once())
                ->method('wait');

        // WHEN
        $res = $this->underTest->wait();
    }

    /*
    public function testSetLogger()
    {
        $this->markTestIncomplete();

        $log = new Logger('client');

        $this->underTest->setLogger($log);
    }
    */
}
