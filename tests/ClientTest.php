<?php

namespace BitNinja\NinjaRpc\Test;

use BitNinja\NinjaRpc\CallHandler;
use BitNinja\NinjaRpc\Client;
use BitNinja\NinjaRpc\Encoders\EncoderInterface;
use BitNinja\NinjaRpc\Exception\ResultNotArrivedException;
use BitNinja\NinjaRpc\QueueManagers\QueueManagerInterface;
use BitNinja\NinjaRpc\RemoteCommand;
use Monolog\Logger;

use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    private $underTest;
    private $queueManager;
    private $encoder;
    private $command;
    private $callHandler;

    public function setUp()
    {
        parent::setUp();
        $this->queueManager = $this->createMock(QueueManagerInterface::class);
        $this->encoder = $this->createMock(EncoderInterface::class);
        $this->underTest = new Client($this->queueManager, $this->encoder);
        $this->command = new RemoteCommand('TestService', 'method1', [], 'TestService_method1_aa');
        $this->callHandler = new CallHandler($this->underTest, $this->command, 10);
        $this->encoder->method('decode')->will($this->onConsecutiveCalls($this->command));

        $this->queueManager->method('getNext')->will($this->onConsecutiveCalls($this->command, false));
    }

    /**
     * MethodName_DoesWhat_WhenTheseConditions
     */
    public function testCallInvokeQueueManagerPublish()
    {
        // GIVEN in setUp()
        // WILL
        $this->queueManager->expects($this->once())
            ->method('publish');

        // WHEN
        $res = $this->underTest->asyncCall('pingService', 'ping', ['pong']);
    }

    public function testCallReturnCallHandler()
    {
        // GIVEN in setUp()
        // WHEN
        $res = $this->underTest->asyncCall('pingService', 'ping', ['pong']);

        // WILL
        $this->assertInstanceOf('BitNinja\NinjaRpc\CallHandler', $res);
    }

    public function testWaitInvokeQueueManagerWait()
    {
        // GIVEN in setUp()
        //
        // WILL
        $this->queueManager->expects($this->once())
            ->method('waitWithTimeout');

        // WHEN
        $this->callHandler->wait();
    }

    public function testResultArriveAfterWait()
    {
        // GIVEN in setUp()

        // WILL
        $this->queueManager->expects($this->once())
            ->method('waitWithTimeout');

        $this->assertFalse($this->underTest->isResultArrived($this->command->getCorrelationId()));
        $this->assertFalse($this->underTest->isResultArrived('sdr3dfg'));

        // WHEN
        $this->callHandler->wait();

        $this->assertTrue($this->underTest->isResultArrived($this->command->getCorrelationId()));
        $this->assertEquals($this->command, $this->underTest->getResult($this->command->getCorrelationId()));
    }

    public function testGetResultWichNorArrivedThrowsException()
    {
        // GIVEN in setUp()
        // WILL
        $this->expectException(ResultNotArrivedException::class);

        // WHEN
        $this->underTest->getResult('sfg');
    }

    /*
    public function testSetLogger()
    {
        $this->markTestIncomplete();

        $log = new Logger('client');

        $this->underTest->setLogger($log);
    }
    */
}
