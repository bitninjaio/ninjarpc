<?php

namespace BitNinja\NinjaRpc\Test;

use BitNinja\NinjaRpc\Encoders\EncoderInterface;
use BitNinja\NinjaRpc\QueueManagers\QueueManagerInterface;
use BitNinja\NinjaRpc\RemoteCommand;
use BitNinja\NinjaRpc\Routers\RouterInterface;
use BitNinja\NinjaRpc\Server;
use Monolog\Logger;

use PHPUnit\Framework\TestCase;

class ServerTest extends TestCase
{
    /**
     * @var Server
     */
    protected $underTest;
    /**
     * @var QueueManagerInterface
     */
    private $queueManager;

    /**
     * @var EncoderInterface
     */
    private $encoder;

    /**
     * @var RouterInterface
     */
    private $router;

    protected function setUp()
    {
        parent::setUp();
        $this->queueManager = $this->createMock(QueueManagerInterface::class);

        $this->encoder = $this->createMock(EncoderInterface::class);
        $this->encoder->method('decode')->willReturn(new RemoteCommand('TestService', 'method1', [], 'TestService_method1_aa'));

        $this->router = $this->createMock(RouterInterface::class);

        $callback = function ($param1) {
            return $param1;
        };

        $this->router->method('getServices')->willReturn(['method1' => $callback]);

        $this->underTest = new Server('TestService', $this->queueManager, $this->encoder, $this->router);

        $command = new RemoteCommand('TestService', 'method1', [], 'TestService_method1_aa');
        $this->queueManager->method('getNext')->
                will($this->onConsecutiveCalls($command), false);
    }

    public function testListenInvokesQueueManagerWaitWithTimeout()
    {
        // GIVEN in SetUp()

        // WILL
        $this->queueManager->expects($this->once())
                ->method('waitWithTimeout');

        // WHEN
        $this->underTest->listen();
    }

    public function testListenInvokesQueueManagerGetNext()
    {
        // GIVEN in SetUp()

        // WILL
        $this->queueManager->expects($this->once())
                ->method('getNext');
        // WHEN
        $this->underTest->listen();
    }

    /*
    public function testSetLogger()
    {
        $this->markTestIncomplete();

        $log = new Logger('client');

        $this->underTest->setLogger($log);
    }
    */
}
