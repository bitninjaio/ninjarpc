# NinjaRPC #

The easy to use queue based RPC library. You can use any queueing system and 
any ancoding to connect your server and client. With ninjaRPC you can setup
your PHP based microservice stack.

## Installation

```
composer require bitninja/ninjarpc ~2.1
```


## Example Server

```

use BitNinja\NinjaRpc\Server;
use BitNinja\NinjaRpc\Encoders\JsonEncoder;
use BitNinja\NinjaRpc\QueueManagers\RabbitMQ;

// Target service
$ping = function($param1){
    echo 'ping was called!';
};

// Set up a queue manager
$queueManager = new RabbitMQ();

// Set up an encoder
$encoder = new JsonEncoder();

// Set up a router
$router = new SimpleRouter([
    'ping' => $ping
]);

// Create the server
$server = new Server('TestServer', $queueManager, $encoder, $router);

// Serve requests forever
while (1) {
    $server->listen();
}

```

## Example Client

```

use BitNinja\NinjaRpc\Client;
use BitNinja\NinjaRpc\Encoders\JsonEncoder;
use BitNinja\NinjaRpc\QueueManagers\RabbitMQ;

// Set up a queue manager
$queueManager = new RabbitMQ()

// Set up an encoder
$encoder = new JsonEncoder();

// Create the client
$client = new Client($queueManager, $encoder);

// Invoke an rpc call
$call = $client->asyncCall('TestServer', 'ping', ['param1' => 'pong']);

// Wait for the response
$call->wait();

// Debug dump the results
$call->dumpResult();

```

## Motivation

Recently microservices has more and more popularity, and one of the best
way for your services to communicate with each other is throug message queues.
There are a lot fo different queueing systems. The most famous general puprose
queueing ssgem is RabbitMQ, but you can use redis, ActiveMQ, Kafka, etc. 

NinjaRPC manages all the nasty details behind the scenes of an RPC call using
a message queue. Doing an RPC call is not simply sending a message. You have to
deal with the response too, take care of declaring the queues, handle timeouts, 
etc. This is where NinjaRPC helps you.

You can use the built in queue implementations or create your own 
implementation. (please send me a pull request if you implement new queuing systems)

Also you have the freedom to choose a coding. I have implemented some basic 
encoders like the PHP built in serialize and json, but you are free to implement 
any other encoders to us.


## API Reference



## Tests

### Unit tests

You can run the unit tests using phpunit. Simply run it at the root of the 
git source tree, and it will run all the unit tests and generate shiny reports.

### Integration tests

You can run a basic integration test. Make sure to set up a rabbitmq server
with default configurations, or start a docker for it first.

```
$ docker run -d --hostname my-rabbit --name some-rabbit rabbitmq:3
```

Then you can start the integration tests. Start with the server.

```
$ cd example
$ php ./server.php
```

Then in a new console start the client too.

```
$ cd example
$ php ./client.php
```


## Contributors

Any contribution is very welcome! If you implement new queue drivers or 
encoders, please send a pull request.

## License

MIT license.
