#!/usr/bin/php
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$loader = require dirname(__DIR__) . '/vendor/autoload.php';

use \Bramus\Monolog\Formatter\ColoredLineFormatter;
use BitNinja\NinjaRpc\Encoders\PhpEncoder;
use BitNinja\NinjaRpc\Event\Emitter;
use BitNinja\NinjaRpc\Event\Handlers\ZipkinEventHandler;
use BitNinja\NinjaRpc\Event\Handlers\ZipkinManager;
use BitNinja\NinjaRpc\QueueManagers\RabbitMQ;
use BitNinja\NinjaRpc\Routers\SimpleRouter;
use BitNinja\NinjaRpc\Server;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

// Set up the logger
$log = new Logger('client');
$handler = new StreamHandler('php://stdout'); //, Logger::WARNING);
$handler->setFormatter(new ColoredLineFormatter());
$log->pushHandler($handler);

$ping = function ($param1) {
    static $counter = 0;

    echo 'ping was called!';
    sleep(1);

    return $counter++;
};

// Set up the queue manager
$queueManager = new RabbitMQ();
$queueManager->setLogger($log);

// Set up the encoder
$encoder = new PhpEncoder();

// Set up the router
$router = new SimpleRouter([
    'ping' => $ping
]);

// Set up a zipkin event handler
$zipkinManager = new ZipkinManager('http://10.42.35.194:9411/api/v1/spans');
$eventHandler = new ZipkinEventHandler($zipkinManager, true);
$emitter = new Emitter();
$emitter->register($eventHandler);

$server = new Server('TestServer', $queueManager, $encoder, $router, $emitter);
$server->setLogger($log);

while (1) {
    $server->listen();
}
