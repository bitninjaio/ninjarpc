#!/usr/bin/php
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$loader = require dirname(__DIR__) . '/vendor/autoload.php';

use \Bramus\Monolog\Formatter\ColoredLineFormatter;
use BitNinja\NinjaRpc\Encoders\PhpEncoder;
use BitNinja\NinjaRpc\QueueManagers\RabbitMQ;
use BitNinja\NinjaRpc\Routers\SimpleRouter;
use BitNinja\NinjaRpc\Server;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

$log = new Logger('client');
$handler = new StreamHandler('php://stdout'); //, Logger::WARNING);
$handler->setFormatter(new ColoredLineFormatter());
$log->pushHandler($handler);

$ping = function ($param1) {
    static $counter = 0;
    echo "Ping arrived. Waiting 3 secs\n";
    sleep(3);
    echo "3 sec passed.\n";
};

$queueManager = new RabbitMQ();
$queueManager->setLogger($log);

$encoder = new PhpEncoder();
$router = new SimpleRouter([
    'ping' => $ping
]);

$server = new Server('TestServer', $queueManager, $encoder, $router);
$server->setLogger($log);

while (1) {
    $server->listen();
}
