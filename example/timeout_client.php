#!/usr/bin/php
<?php

/*
 * Integrity test for long RPC calls.
 */

$loader = require dirname(__DIR__) . '/vendor/autoload.php';

use \Bramus\Monolog\Formatter\ColoredLineFormatter;
use BitNinja\NinjaRpc\Client;
use BitNinja\NinjaRpc\Encoders\PhpEncoder;
use BitNinja\NinjaRpc\QueueManagers\RabbitMQ;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

// Set up the logger
$log = new Logger('client');
$handler = new StreamHandler('php://stdout'); //, Logger::WARNING);
$handler->setFormatter(new ColoredLineFormatter());
$log->pushHandler($handler);

// Set up the queue manager
$queueManager = new RabbitMQ();
$queueManager->setLogger($log);

// Set up the encoder
$encoder = new PhpEncoder();
$encoder->setLogger($log);

// Create the client
$client = new Client($queueManager, $encoder);
$client->setTimeout(2);
$client->setLogger($log);

// Do a call
$time_start = microtime(true);
 $call = $client->asyncCall('TestServer', 'ping', ['param1' => 'pong']);
$time_call = microtime(true);

// Wait for response
$call->wait();
$call->dumpResult();
$time_result = microtime(true);

// Log the measurements.
$log->info('The call took ['.($time_call - $time_start).'] seconds');
$log->info('Waiting for the result took ['.($time_result - $time_call).'] seconds');
$log->info('The whole was ['.($time_result - $time_start).'] seconds');
