#!/usr/bin/php
<?php

/*
 * Integrity test for long RPC calls.
 */

$loader = require dirname(__DIR__) . '/vendor/autoload.php';

use BitNinja\NinjaRpc\Client;
use BitNinja\NinjaRpc\Encoders\PhpEncoder;
use BitNinja\NinjaRpc\Event\Emitter;
use BitNinja\NinjaRpc\Event\Handlers\ZipkinEventHandler;
use BitNinja\NinjaRpc\Event\Handlers\ZipkinManager;
use BitNinja\NinjaRpc\QueueManagers\RabbitMQ;
use Bramus\Monolog\Formatter\ColoredLineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

// Set up the logger
$log = new Logger('client');
$handler = new StreamHandler('php://stdout'); //, Logger::WARNING);
$handler->setFormatter(new ColoredLineFormatter());
$log->pushHandler($handler);

// Set up the queue manager
$queueManager = new RabbitMQ();
$queueManager->setLogger($log);

// Set up the encoder
$encoder = new PhpEncoder();
$encoder->setLogger($log);

// Set up a zipkin event handler
$zipkinManager = new ZipkinManager('http://10.42.35.194:9411/api/v1/spans');
$eventHandler = new ZipkinEventHandler($zipkinManager, true);
$emitter = new Emitter();
$emitter->register($eventHandler);

// Create the client
$client = new Client($queueManager, $encoder, $emitter);
$client->setLogger($log);

$time_start = microtime(true);

$call = $client->asyncCall('TestServer', 'ping', ['param1' => ['a' => 'pong']]);

$time_call = microtime(true);

$call->wait();
$call->dumpResult();
$time_result = microtime(true);

$log->info('The call took ['.($time_call - $time_start).'] seconds');
$log->info('Waiting for the result took ['.($time_result - $time_call).'] seconds');
$log->info('The whole was ['.($time_result - $time_start).'] seconds');
