#!/usr/bin/php
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$loader = require dirname(__DIR__) . '/vendor/autoload.php';

use \Bramus\Monolog\Formatter\ColoredLineFormatter;
use BitNinja\NinjaRpc\Client;
use BitNinja\NinjaRpc\Encoders\PhpEncoder;
use BitNinja\NinjaRpc\QueueManagers\RabbitMQ;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

// Set up the logger
$log = new Logger('client');
$handler = new StreamHandler('php://stdout'); //, Logger::WARNING);
$handler->setFormatter(new ColoredLineFormatter());
$log->pushHandler($handler);

// Set up he queue manager
$queueManager = new RabbitMQ();
$queueManager->setLogger($log);

// Set up the encoder
$encoder = new JsonEncoder();
$encoder->setLogger($log);

// Create the client
$client = new Client($queueManager, $encoder);
$client->setLogger($log);

$time_start = microtime(true);

for ($i = 0; $i < 100; $i++) {
    $calls[$i] = $client->asyncCall('TestServer', 'ping', ['param1' => 'pong']);
}

$time_call = microtime(true);

foreach ($calls as $call) {
    $call->wait();
    $call->dumpResult();
}
$time_result = microtime(true);

$log->info('The call took ['.($time_call - $time_start).'] seconds');
$log->info('Waiting for the result took ['.($time_result - $time_call).'] seconds');
$log->info('The whole was ['.($time_result - $time_start).'] seconds');
